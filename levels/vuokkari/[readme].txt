Vuokkari Stadium

by Ahma and Zeino :)

Length: 1517 m (1532 m in reversed)
Challenge time: 2:00:000 (2:05:000 in reversed) with pro/super pro car
Made for the Re-Volt World 2nd Birthday Contest.
The track should have been something similar to Wario Stadium DS but it ended up being a winter stadium track in the ridge of Soppee where "Vuokkari": The mysterious capital of finnish Re-Volt community keeps spawning new world records and session ideas for racers around the world.

Credits

Ahma: Main creator
Zeino: Bug fixes, 3d modelling, most of the textures
Kiwi: Falling snow animation
Alexander: Some textures
