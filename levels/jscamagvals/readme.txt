Name: Magnum Valley 2
Authors: Josh Scorpius
Length: 1,042 metres (0.647 miles)
Released on: Wednesday, April 17, 2019.
Updated on: Tuesday, April 23, 2019.

This is the second version of the Magnum Valley, an track with some bumps, trees, walls, platforms, etc. The second version is much longer with some other areas to discover in this dark brown valley.

Contains:
Pickups
Triggers
Custom camera nodes
Challenge time (1:12:000)
Reversed mode
Music (Sweetland, 4 minutes 13 seconds)

Any case, contact at: 
Josh Scorpius: joshscorpius94@gmail.com