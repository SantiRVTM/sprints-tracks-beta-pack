Information
-----------------
Track Name:	Touge Mountain
Length:		2502 m (Normal) / 2440 m (Reversed)
Difficulty	Hard
Author:		Saffron


Description
-----------------
The first fully-functioning point-to-point track for Re-Volt.

Ripax discorvered how to trick the game into making point-to-point tracks possible many years ago. At the time I always wanted to make a track that made use of the concept, but my skillset at the time was lacking, and my limited creativity didn't allow me to think of anything meaningful.

When the community got revitalised thanks to the Discord server and RVGL, online drifting sessions made a small comeback for the first time in 5-6 yeras. One of the many trackes we drift(ed) on was Touge Drift by MOH. Always a fan of its purely downhill nature, theme and aesthetic, I was often disappointed in how short it was - which was how I slowly started thinking about making my own take on Touge Drift.

The initial raceline was twice as long and took over 10 minutes to complete with a drift car, so I cut the track in half. However, with no semblance of a plan on how the track would look, I quickly realised it'd be best to build up my skills in Blender before properly working on this, so I focused on other projects instead.

The track was restarted numerous times, and at one point it became a track based on a real-life road in Japan, but not once were they thoroughly planned besides their raceline, so my motivation to continue working on them would quickly fizzle out.

In late spring of 2019, I finally decided to revisit the track again, restarting the entire process, and carefully planning how the environment would look with the raceline, and keeping the elevation changes moderate. This resulted in a much more streamlined, functional and realistic raceline than the original, and laid a solid foundation for which I could get the project off the ground for good.

August 2019 I picked up the project again and put the initial version of the track into the game. There was no scenery, simply a road with some grass, with concrete and a forest texture as the barriers.

Testing it online with others showed some keen interest, so I began working on it continously by fleshing out the basic scenery, placing the lampposts and trees, revisiting the textures and visuals, scaling up the track to make it friendlier for drifting, and finally finishing it off.

However, in the midst of that I ran into some issues relating to position nodes that I will never know the answer to, so I had to turn to Huki to help make point-to-point tracks possible. They were swift in working towards a solution, and the rest is history.

The latest version of RVGL is required for the track to function properly.


Credits
-----------------
Polyphony Digital, Lo Scassatore, Marv and the rest of the Internet for the textures
T-Rider, Kirioso and Odie for the music
Blender Foundation for Blender
Marv, Martin and Huki for the Blender plug-in
Huki, Marv and FranklyGD for RVGL
Kiwi, Marv and R6TE for finding the sound effects used
ARM for the camera nodes tutorial
Rick Brewster for Paint.NET
Everyone in Re-Volt Discord for testing out of the track and giving feedback


Permissions
-----------------
This track cannot be distributed anywhere except my website, or the I/O Circuit Pack.