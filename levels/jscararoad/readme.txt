Name: JSCA Rainbow Road
Authors: Josh Scorpius
Length: 383 metres (0.238 miles)

This is the Rainbow Road version I created with an narrow 180 turn and some obstacles, this track is an point to point race.

Contains:
Pickups
Custom camera nodes
Reversed mode

Any case, contact at: 
Josh Scorpius: joshscorpius94@gmail.com