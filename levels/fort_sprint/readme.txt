
███████╗░█████╗░██████╗░████████╗██████╗░███████╗░██████╗░██████╗  ░██████╗██████╗░██████╗░██╗███╗░░██╗████████╗
██╔════╝██╔══██╗██╔══██╗╚══██╔══╝██╔══██╗██╔════╝██╔════╝██╔════╝  ██╔════╝██╔══██╗██╔══██╗██║████╗░██║╚══██╔══╝
█████╗░░██║░░██║██████╔╝░░░██║░░░██████╔╝█████╗░░╚█████╗░╚█████╗░  ╚█████╗░██████╔╝██████╔╝██║██╔██╗██║░░░██║░░░
██╔══╝░░██║░░██║██╔══██╗░░░██║░░░██╔══██╗██╔══╝░░░╚═══██╗░╚═══██╗  ░╚═══██╗██╔═══╝░██╔══██╗██║██║╚████║░░░██║░░░
██║░░░░░╚█████╔╝██║░░██║░░░██║░░░██║░░██║███████╗██████╔╝██████╔╝  ██████╔╝██║░░░░░██║░░██║██║██║░╚███║░░░██║░░░
╚═╝░░░░░░╚════╝░╚═╝░░╚═╝░░░╚═╝░░░╚═╝░░╚═╝╚══════╝╚═════╝░╚═════╝░  ╚═════╝░╚═╝░░░░░╚═╝░░╚═╝╚═╝╚═╝░░╚══╝░░░╚═╝░░░


================
Main information
================
Track authors: rodik, Ahma
Track length: 749m
Time Trial time: 1:00:000 
Reverse Version: no
Practice star: yes
Track difficulty: Hard

=============
Track Testers
=============
Ahma
CapitaneSZM
Honk
MightyCucumber
Powerate
Tubers
Xarc
VaidX47
Zeino

===============
About the track
===============
The track was made for Re-Volt World Birthday Contest 2023. 
I was inspired by the track Castle 2 by Xarc.

So my idea was as follows: You're driving around in some kind of fortress. 
Because of its age, it began to fall apart. Fallen bridges, debris, etc.
But it has not lost its magic, and the longer you drive, 
the more you will see mechanisms that lift parts of this track, 
or replace the collapsed ones with new ones.
Moreover, this fortress is flooded.
Also, the further you drive, the harder it rains.

The track name and challenge time was suggested by Ahma.
If you want to see how the track looked without editing, you can open the tdf file, 
using the track editor.

==================
Credits and thanks
==================
Many thanks to Ahma, for such an amazing work done on AI Nodes.
Also thanks to everyone who tested the track and pointed out bugs.
Music was made by JN2002. Thank you very much for that!
The bridge texture is used from the Castle 2 by Xarc.
The water texture is used from the Medieval: Redux by Instant, Kipy.
Waterdrops from Kiwi's Rain Pack were used.
Using FXPage2 Arrow from Wildland by JavilDesign.

==========
Tools used
==========
Blender 2.79b (with Marv's Blender Plugin)
Krita 
Adobe Photoshop CC 2019
WorldCut 11-11-11
Notepad++
In-game MIG
FL Studio 12 and 20

=================
Other information
=================
If you want to use something from this track (it's not much, but still), 
then give credit to me or the authors I mentioned above.

=======
Contact
=======
If you want to contact me, you can do it via Discord - rodik#1750