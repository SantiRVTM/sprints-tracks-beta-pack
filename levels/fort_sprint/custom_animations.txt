
MODEL   0   "model1"
MODEL   1   "model2"
MODEL   2   "model3"
MODEL   3   "bridge1"
MODEL   4   "bridge2"
MODEL   5   "model4"
MODEL   6   "chain"

SFX     0   "sound1"


ANIMATION {
  Slot                      0                         ; Slot ID (0 to 255)
  Name                      "Bridge"                   ; Display name (currently unused)
  Mode                      0                         ; 0: loop / 1: animate a single time, stopping after the last frame / 2: back and forth (reverse after last keyframe)

  BONE {
    BoneID                  0                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 3                         ; Model ID (0 to 63, or -1)
  }
  

 KEYFRAME {
    FrameNr                 0
    Time                    1.700                  
    Type                    2                             ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
    BONE {
      BoneID                0
      RotationAxis          0.000 0.000 1.000         ; Rotation axis
      RotationAmount        0.500                  ; Rotation (in degrees)
    }
  }

KEYFRAME {
    FrameNr                 1
    Time                    3.400                  
    Type                    3                             ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
    BONE {
      BoneID                0
      RotationAxis          0.000 0.000 1.000         ; Rotation axis
      RotationAmount        -1.000                  ; Rotation (in degrees)
    }
  }

KEYFRAME {
    FrameNr                 2
    Time                    1.700                  
    Type                    1                             ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
    BONE {
      BoneID                0
      RotationAxis          0.000 0.000 1.000         ; Rotation axis
      RotationAmount        0.500                  ; Rotation (in degrees)
    }
  }
}

ANIMATION {
  Slot                      1                         ; Slot ID (0 to 255)
  Name                      "Model 1"                   ; Display name (currently unused)
  Mode                      1                         ; 0: loop / 1: animate a single time, stopping after the last frame / 2: back and forth (reverse after last keyframe)
  NeedsTrigger	 	    true

  BONE {
    BoneID                  0                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 0                         ; Model ID (0 to 63, or -1)
  }

 KEYFRAME {
    FrameNr                 0
    Time                    7.0                  
    Type                    3                             ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
    BONE {
      BoneID                0
      Translation           0.000 1125.000 0.000       ; Translation
    
	 SFX {
          SfxID               0
          Range               0
          Looping             false
       }
     }
  }
}

ANIMATION {
  Slot                      2                         ; Slot ID (0 to 255)
  Name                      "Model 2"                 ; Display name (currently unused)
  Mode                      1                         ; 0: loop / 1: animate a single time, stopping after the last frame / 2: back and forth (reverse after last keyframe)
  NeedsTrigger	 	    true

  BONE {
    BoneID                  0                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 -1                         ; Model ID (0 to 63, or -1)
  }

  BONE {
    BoneID                  1                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 1                         ; Model ID (0 to 63, or -1)
    OffsetTranslation       00.000 -1100.000 00.000
    OffsetRotationAxis      0.000 1.000 0.000        ; Offset rotation axis
    OffsetRotationAmount    90.000  
  }

BONE {
    BoneID                  2                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 5                         ; Model ID (0 to 63, or -1)
  }

KEYFRAME {
    FrameNr                 0
    Time                    2.0                  
    Type                    3                             ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
    BONE {
      BoneID                1
      RotationAxis          0.000 1.000 0.000         ; Rotation axis
      RotationAmount        -90.000                  ; Rotation (in degrees)
    }
  }
  

 KEYFRAME {
    FrameNr                 1
    Time                    2.5                  
    Type                    3                             ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
    BONE {
      BoneID                1
      Translation           0.000 1100.000 0.000       ; Translation
    }

BONE {
      BoneID                2
      Translation           0.000 1100.000 0.000       ; Translation
    }
  }
}

ANIMATION {
  Slot                      3                         ; Slot ID (0 to 255)
  Name                      "Finish"                   ; Display name (currently unused)  
  Mode                      2                         ; 0: loop / 1: animate a single time, stopping after the last frame / 2: back and forth (reverse after last keyframe)

  BONE {
    BoneID                  0                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 2                         ; Model ID (0 to 63, or -1)
    OffsetRotationAxis      0.000 1.000 0.000        ; Offset rotation axis
    OffsetRotationAmount    -90.000  
  }
  

 KEYFRAME {
    FrameNr                 0
    Time                    5.000                  
    Type                    3                             ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
    BONE {
      BoneID                0
      Translation           0.000 500.000 0.000       ; Translation
    }
  }
}


There should have been another animation (even 2), but it's not there.