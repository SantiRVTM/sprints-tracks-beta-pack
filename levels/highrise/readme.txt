//-- LOG START --\\

// H I G H R I S E \\

// A Lego track by yun (with the help of EstebanMZ) \\

Made with Dummiesman's Unity Track Editor + Blender

A short Lego sprint track with the purpose of using every tile possible, but not for the raceline exclusively.
The aim of the track is to find as many shortcuts as possible, many of which rely on abusing track geometry and are plenty obvious. Originally I had planned a way bigger hillclimb track but ended up frustrated due to how bug happy sprint mode really is, and overall dislike for it.

-With anger and vitriol, yun

Credits:

yun:

Initial AI Nodes, POS Nodes, Track Zones placement
Track idea
Time spent on it
Somehow fixing the unfixable by sheer luck
Finding a good .mod file to use in the track

EstebanMZ

Going out of his way to help me
Further tweaking of the nodes and track zones
Helping with some Blender magic
Retouching (finishing) track_dir triggers
Aspiring to leave Colombia

ModArchive for providing the tracker file that I converted to mp3 (badlands1.mod) @ https://modarchive.org/index.php?request=view_by_moduleid&query=34963

// Update v2 \\

Removed the "start loop" trick to be able to export the track (completely missed it)
Made the camera at the start position not collide with a wall, replacing the initial "Jump" modules with a nice checkerboard courtesy of EstebanMZ
Somehow removing an annoying repo bug the AI had

//-- LOG END --\\