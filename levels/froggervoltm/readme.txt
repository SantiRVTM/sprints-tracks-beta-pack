
>>>>>>>>>>>>>>
>  General
>>>>>>>>>>>>>>
	Track Name: 			MiniGame: FroggerVolt Hard
	``````````                         
	Theme:				Arcade
	``````                         
	Folder Name:			froggervoltm
	```````````` 
	Maker(s):		 	Kallelay "kay" 
	`````````
	Concept:			Early concept 2010: Urnemanden and kay
	````````			New concept 2020:   Looping planch idea from Razzoy and Hajducsekb

	Track Type:			Extreme 
	```````````
	Track Length:			65m
	`````````````
	Track Type:			NoRepo Sprint / Frogger
 	```````````




 ___________
(   Intro   ) 
 ```````````
 10 years ago (June'15th, 2010), Urenamnden and I were talking about one of his fav games (Frogger) and it would have been nice to make one for Re-Volt.
 The basic concept (train) was working, however, the thrill was not good, and the project was soon damped.
 10 years later, I was looking for an "ice breaker" to motivate me continue working on the tracks and found this.

 So, the textures are based on a track of mine "Hokkaido House" (and has one acclaim texture), the concept was redone in a few minutes and the pipeline for making the track were planned and finished today 5:49 PM 2020-08-11.

Fixed on 2020-08-17!
 
 
 ____________
(Requirements)
 ````````````
 - RVGL 2020.04.30


 ___________
(Dedication ) 
 ```````````
I'd like to thank everyone in Re-Volt Community! Especially #track discord team!
A special thanks to Last Cuban for my fav RV Track Guide! http://www.revoltxtg.co.uk/
 as well as for pointing the error out!


 ___________
(   Link   ) 
 ```````````
http://www.revoltxtg.co.uk/msf/nutrkx/frog_hard.htm


 _____________
(  Tools used )
 `````````````
modeling: 3Ds max 2011
********
Textures: Photoshop, Paint.NET, GIMP, 3Ds max 2011
********
Previous Exporting: RVTMOD8 (sunpos=-9), RVGL
******************
New Exporting: Blender + Marv's plugin + TextAnimYUI + SunShade [sunpos=-9] 
**************

