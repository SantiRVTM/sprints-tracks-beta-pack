                  ****************************************************************
                  * ~ Re-Volt Abduction - Episode 1: On a Dark and Rainy Night ~ *
                  ****************************************************************
                   
                  "Testimonies should have alerted us, but we didn't believe it!"
                  
                  
_________________________________________________                  
 
Here is the first track of my second original cup: "Re-Volt Abduction".

Author: CED!
Creation: October 2023
Category: Sprint Track                  
Length: 384 meters           
Difficulty: medium        
Practice Star: 1
Time Trial: 00:22:622
 _________________________________________________
  
This track is based on some tiles from the Rally PRM Set by Keyran which I slighty resized. I also used the alien, wirecone & UFO from the track "Unit 51" by Josh PIN, the trees from "Morso Cave" by Ahma & Rodik, the lamps from Downtown by G_J, I_Spy & Kipy and the medical equipment from "Hospital" by  G_J, I_Spy & MightyCucumber.

The rain and storm effects come from Makeitgood mode and especially from the "Kiwi Rainpack".

The sounds come from the original game (and some have been modified), except the one I use for what I will call the "Alien Communication" which comes from Joseph Sardin's site https://lasonotheque.org/.

Music is an extract from "The Old Chisholm Trail" by Yodelin' Slim Clark. I quote Wikipedia: "This file is in the public domain, because it is a recording based on a folkloric or traditional musical composition which has fallen out of copyright and any recording rights to this version have expired."
For more information on this song: https://en.wikipedia.org/wiki/The_Old_Chisholm_Trail
__________________________________________________

This "readme file" was achieved with the help of an online translator. Sorry if English is approximate, it would have been worse without.

I hope you enjoy this track,
thank you for downloading it (-;

Thanks to 607, Keyran & Rodik for testing this track and for the feedback.

Special thanks to Acclaim, the RVGL Team, all tracks & cars creators, and all other Re-Volt contributors, past, present & futur...

Don't use this track or its content for commercial purposes.
You can reuse any part of this track provided you mention the original authors.
__________________________________________________
