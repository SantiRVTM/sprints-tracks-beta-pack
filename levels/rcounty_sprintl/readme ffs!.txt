==========================================================
= QUICKSTART, read this if you're the TLDR kind of guy ;) 
==========================================================

Track name: Ruddles County.
Track creators: 
- Lo Scassatore; lead modeler.
- FBV-86; concept creator, helper modeler, texture and sound tech.
Helper(s): 
- Kratos; 3d prefab provider.
- Burner94; beta tester.
Track lenght: 5796 metres.
Thanks to Huki for connecting the pos nodes of the old circuit correctly.
Track type: Experimental, free roaming\sandbox, now with standard circuit mode.

You can fool around alone for hours in this 
map, but it really shines for online games...

IMPORTANT: this map is better enjoyed with the 
game Instances set to active, deactivating them may
break the gameplay of this map in several places.

===================================================================
= DETAILED README = Just in case you're curious about this mess...
===================================================================

This is an updated version of the old Ruddles County, the very first
Re-Volt free-roaming\sandbox map. This project was conceived in early
2014, to be a tribute to the Midtown Madness map "Offroad Madness".

In 2014 I had little to none knowledge about 3d modeling, so I commissioned
this project to Lo Scassatore ( ballsy cars and tracks creator ) and it
went on and on, eventually becoming more polished and articulated throughout
the time. The main goal was to create to create a vibrant scenario for cops
and bandits chases, and also a bit of long-haul trucking in the mix.
Of course, you can drive anything on this map and have fun, exception for
super fast cars or big scale cars since they're way out of context here...

The variety of the map allows for endless gaming possibilities, you can
make up your own time attacks, go offroading in the swamps, rock crawling
on the mountains, drifting through the SS32, or simply, put up a lobby 
and go explore with your friends, there are many secrets and hideouts
spread all over the county waiting to be uncovered by you! 

======================================================================
= DEV NOTES & BRAGGING, don't take the bragging part too seriously ;) 
======================================================================

- Heaviest RV track ever made: its .ncp file weighs around 5.5 megabytes.
  It takes several seconds to load in RV, and several minutes to export 
  from Blender...emphasis on SEVERAL!

- For that matter, the polygons on the .w and .ncp files are not completely
  triangularized, otherwise they would weigh alot more. This may result in 
  collision and visual glitching in some minor areas, nothing game-breaking
  though.

- All texture files are ramped-up to 1024x1024 res, and still, it was 
  impossible to get decent textures for everything. Sadly many contents
  have been cut due to this costraint, and even more sadly, some people
  still do not get this concept.

- The whole map is 5 times the toytanic in lenght, and 4 times the
  toytanic in width, for an extimated 11 square miles of driveable
  terrain. And roughly 334 camera nodes have been placed to get a
  view of every location, yea, it's a little big ;)

- Pretty much the longest racing path ever in RV ( 5796 metres ) the older
  layout fixed by Huki was 5723 metres long. Also, the old layout used up 
  all of the AI nodes Re-Volt is able to process. Literally, the MIG did 
  not allow to place more of them. Even though I still think this map is 
  not by any means accomodating for a race course, I must admit it's pretty
  cool to have single race support for the Ruddles County.

- Uses almost every instance of surface material ( gravel, grass, glass, sand etc )

- RV won't allow to place more solid instances.
  ( .fin file weighs 47 kb, Museum1's .fin file weighs 9 kb for size )
  It is possible to "visually" place a solid instance, but the game 
  won't process its collision file. I can't be bothered to check out 
  how many solid instances I've placed on this thing. Also I don't 
  think the sheer number of solid instances is to matter, rather than
  their polycount ( .ncp volume ) also, I'm not sure if non-solid instances
  actually affect the maximum number of solid instances allowed per racetrack.

- I once tried to scale this thing up to real size...regretted it immediately...

============================================
= SO, WHAT'S NEW FROM THE PREVIOUS RELEASE?
============================================

Quite a lot actually...

- Custom reversed layout.
- Reworked, and new textures.
- New hideouts for the bandits.
- New light effects in the tunnels and few other areas.
- New roads, leading to new locations, mostly panoramic points, which offer their unique sights.
- The brand new Bon'er woods area, accessible via tunnel from the SS32 and via secondary route on
  the righthand path of the Mount Sfrangiolive. This new access route is not suitable for heavy vheicles.
  This area encircles the Mount Sfrangiolive, and it was created to prevent the cars from falling off the map.
  Eventually it has been setted-up as a wood hauling scenario, but it can be also used to drift rally cars :)
  Also, adds up to potential hideout points for the bandits\robbers, and allowed to define the racing course 
  of the "Ruddles County Grand Prix". If speed is not your gig, there are plenty of mud holes in there...
- The Ardesia Cave has been reworked to be wider, also, it has 2 levels now instead of 3.
  It gets double access to the bottom level though, making it more interesting for chasing scenarios.
  Some dirt mounts and a hopper have been added for the pleasure of them offroaders.
  Most of the semi trucks could have issues in trying to get out.
- The Chill Glade has been reworked to look more like an actual swamp, complete with mushy waters.
  The water actually dampens your car capability to steer and put down traction, especially lighter cars.
- Reworked map and roads boundaries, to prevent players to fall-off the map, or simply cut across.
- Fixed several camera glitches due to bad instances placement and square faces.
- The Gaper Mine has a proper tunnel network now, instead of being just a hideout.
  Now you can use its tunnels to get back on the road if you accidentally fall down into the rocky chasm.
- Franco Alonge's Carrozzeria is now an accessible building.
- Added an extension to Broil Park, featuring a waterfall ( sadly not animated )
  The new area is accessible via tunnel on the SS32.
  It also features a set of platforms which can be used as an alternate way to get back on the SS32.
- The ruins now have a couple of accessible rubble towers, and improved rubble ramps.
  So now there are a few more hiding spots and easier access between the yards.
- Revomed 'glossyness' from many custom objects such as boulders, roadblocks, and the pigs.
- Revised powerups and gold bar placement.
- BGM.mp3 file has been enriched with new soundtracks.
  Now worth for a 25 minutes rollercoaster of laidback and upbeat country music!
- Added some new road signs to help you navigating through this mess!
- Changed fog tint saturation for a more realistic effect.
- Added a small ramp in the pigs corral, so you can jump out of it more easily.
- Air Balloons are bigger now, wow! This has to be the best new feature yet!

======================================
= COPS VS ROBBERS GAMEPLAY EXPLAINED
======================================

Since this map was primarly built around this concept, its worth spending some 
time learning the basics and the rules of this un-supported game mode.

Being "un-supported" means you have to rely on fair play and coordinations
between the players since Re-Volt has no means of keeping the game under 
control. So a voicechat software such as teamspeak or skype is highly 
advised to rapidly and efficently swap communications between the players.
Which is the core mechanic of this game mode.

Powerups must be activated as they also are a core mechanic of this game mode.

A group of players gets set to play as cops, they can choose any of the
cop vehicles from the ANM car roster, a varied car roster can make easier
of a job on catching the robbers. Then, one or two players ( depending on
how many cops are present on the map ) can pick a car suited with a 
performance index within the one of the cop cars, meaning they should
not pick a car too much superior or inferior to the others.

Of course, the two factions should be in separated voicechat rooms.

As the countdown ends, the robbers must scatter, trying to lose the
tail, as some of the cops try to keep up with them, these cops have
the task of feeding informations about the robbers' locations to the
other groups of cops whom should gather powerups from the closest
source ( the best being the fuel depot in Ruddles Town ) untill
they get an electro pulse, which must be used to stop a robber's
car. If there are multiple robbers, the ones caught must not interfere
with the game, and held prisoner by the police station in Ruddles Town.

The turndown of this mode is Re-Volt has no means of cycling through
the cop and robber players, so, if the robber players gets fed up, a
re-host of the game is in order.

===================================
= POTENTIAL AREAS & GAMEPLAY TIPS 
===================================

Battle Tag, THE WHOLE MAP: gold bars are scattered
all over the landscape. Gold bars work exactly as 
the default stars. You might even find fun trying
to collect them all in offline mode ( practice mode )

Toshinden arena, IL RENE'S RUINS: this one is kinda
more difficult since the grassy surface of the little
island is very slippery. Also, mind those geysers 
since they got enough power to send a car flying.

Another Toshinden arena, PANORAMA PLAZA: use the
ramp to get on the roof the plaza, this arena is
the smallest of the lot, very difficult.

Car Soccer, PURCELL'S FARM: the area with the two
big barns near the monster jam oval, they can be 
used as goals, all you need to do, is to  get yourself
one of the giant boulders and you're good to go!

Monster Jam, PURCELL'S FARM: an aggressively banked
dirty little oval with a jump on the first straight
fun to mess around with dirtbikes, buggyes, city buses
and monster trucks themselves to name a few...

Gravelmania Hill, PURCELL'S FARM: the gravel mount is 
actually irregular so it poses a real challenge to the
players as they need to steer-paddle in order to keep
the truck straight. Of course, you have to get on top
of it without any runup.

Mud Bogging, PURCELL'S FARM: the two pits are also 
functional as they have an irregular bottom, well
timed corrections and wise positioning of your car is 
crucial to beat your opponent to the other end of the pit.

SuinCide Jump, PURCELL'S FARM: there's no actual skill
involved in this challenge, it is fun though, you can
also adjust your angle of approach to pull some stunts!

Permanent circuit, CHILL GLADE: nothing better than an
old fashioned race in the dirt, but since the map's AI
nodes and such are used for a different circuit, you'll
have to rely on a gamekeeper to mantain track of the 
race results. You can also just fool around in the bog
but be careful, light vehicles are very likely to float
thus getting permanently and utterly stuck.

Cannonball Rally, SUNBURNT CANYON: great for point A to
point B races. This is a narrow and nasty patch of dirt
make sure to slow down where needed! Same as for the 
permanent circuit, a gamekeeper is needed.

Another Cannonball Rally, BON'ER WOODS: great for point
A to point B races. A wide but very slippery path, not 
too difficult, unless you're driving a RWD car. 

=============================================================================
== WHERE TO START, WHAT TO SEE == YEA, THIS IS SUPPOSED TO BE A TOUR GUIDE 
=============================================================================

As you start the game, you'll be dropped a few yards away from the little town 
of Ruddles, a vast flat aerea nearby hosts the Purcell's Farm, the most renowed 
Redneck's heaven which features a monster truck jam, a gravely hill, two mud 
pits and a wispy muddy racetrack arranged around the farmer's field, just make
sure to not damage the crop, or the good 'ol Purcell will do some to your ass!

Once you're done with your Rednecky antics, you can either head south, through
route 32 which leads to the majestic Mount Sfrangiolive and the shooting Chill
Glades, or take the dirt road to the west and tackle the unforgiving Sunburnt
Canyon, once theater of bloody feuds between gold prospectors and bandits...now
it's used by truckers to haul wood to the north when the route 31 is flooded, and
one time a year it must bear the violence of the Cannonball Rally of Ruddles.
There's also a small ghost town along the way to see, maybe the gold is still there.

Up to the north, the Highlands is nothing more than a barren land of rocks, with
an amazing paved winding road which will enrapture you as you rocket past the
sneezy town of Saint Indunda, the local handyman makes loads of cash thanks to
the ever present steep cliffs around the curves of the route 32...just make sure
to not end to the bottom of the huge Lake Facicazzo ( don't ask ) before you can
get back to the County of Ruddles, because there's still some stuff left to see.

You can't miss the panoramic spot at Ardesia Peak, right above Saint Indunda.
From there you can get a glipse of the big Lake Facicazzo, the closeby Ardesia
cave and the far away Funnel Woods, take your time to savor the sights!

On the way back to the south you'll see a lovely geotermic area with giant
geysers, they are actually functional, so, if you drive 'em over you'll be
thrown up in the air! Hang time varies in reason of the weight of your car.
Also, you can't miss Il Rene's Ruins, a kidney shaped island which used to
host a big medieval court, now the ruins are just used by locals for battle
tag events and general havoc-fests. Powerboats also race around the island.

Back in south, you can't miss the Chill Glade and the Mount Sfrangiolive if
you didn't take a visit before leaving the county! But make sure to rest at
the Sellerona Motels if you feel winded. Closeby you'll also find the access
to the Bon'er Woods, a dark fertile forest which sustains 60% of the local
economy, and almost encircles the whole Mount Sfrangiolive in its entirety.
This forest also provides access to the righthand path of the Mountain.
The Chill Glade can be accessed by taking a right turn when you see a gravel
path when driving down from the north on the route 32, eventually you'll have
to take a left turn into the tunnel and once out on the other side you'll be
graced with the chill singing of the wind. If you're not the quiet type, you 
can try to set a new speeding record through the Glade...

When done with it, you can head back out of the tunnel and take a left turn,
once to the junction, take another left turn, this will lead you to another
junction with a dirt road on the right, the most elevated scenario in the 
history of Re-Volt awaits you, with its 24.946 game units over the point zero
of the map ( yea nerd stuff, shut up ) it also offers 2 different paths from 
bottom to halfway up, and once to the top you'll be rewarded with a majestic
view of the county, now I dare you to find the fastest way down :D have fun!

And once you took your time fooling around alone in this map, there's such
potential to it as a battle tag arena, also, you can just gather your friends
and cruise or drift around it, and if you think you saw everything of it, you
just might discover a little secret area or some stupid easter egg, who knows?

Anyway, I sincerely hope you find this new experimental kind of racetrack for
Re-Volt at least entertaining, you can send me any feedback on our forums of
Aliasrevoltmaster.com

Have fun fellow Re-Volters - FBV-86