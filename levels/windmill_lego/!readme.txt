﻿Content:
>>> Install Windmills Hills Lego
>>> Track information
>>> Changelog
>>> How to contact me
>>> Description
>>> Additional information
>>> Tools used
>>> Thanks and credits
>>> Disclaimer

Install Windmills Hills Lego:
Unzip Windmills Hills Lego into the main folder of the game.

Track information:
>>> Name: Windmills Hills Lego
>>> Folder name: windmill_lego
>>> Author: Keyran
>>> Year of first release: 2023
>>> Reversed: Yes
>>> Length: 894m
>>> Reversed length: 874m
>>> Categories: Lego Track, Sprint Track
>>> Difficulty: Hard
>>> Time trial challenge: 01:17:00 normal, 01:16:00 reversed
>>> Global star: 1
>>> Practise star: 1
>>> Speedups: 0

Changelog:
Version 1.0.1:
+ Added a screen that plays a video next to the finish line.
+ Changed the time trial challenges.

Contact:
On Discord: I am Keyran#3667.

Description:
All of these windmills in Windmills Hills 1 and 2 provide electricity to a city. After driving for hours and being lost in the Windmills Hills, you finally found that city!

This is a Lego version of my Windmills Hills tracks! And it is a sprint track. All of this for the second birthday contest on Re-Volt World.

The time trial challenge is for SEMI-PRO cars.

Additional information:
This track wouldn't exist if the deadline of the contest wasn't extended. I made the Lego track a few days after the announcement, because I wanted to try Dummiesman's track editor, but then I lost the motivation to continue. I haven't worked on this track for weeks, until the last week before the deadline.
So, this track was rushed, and I didn't expect it to finish 3rd.
Also, the original idea was to make a long downhill track, and the Windmills Hills part should have been longer, but because of the limitations of the track editor (that Dummiesman managed to remove after the contest) and my lack of motivation, most of the track is set into the city.
The video visible next to the finish line has been recorded by myself while playing Venice in Re-Volt Hardcore. It is my first attempt at creating this kind of texture animation.
The start/finish line texture was taken from a cancelled track, Refuge. It was an extremely long (and boring) sprint track, set in a mountain. You can see a screenshot of it in Antigrav SC.

Tools used:
>>> Blender with Marv's plugin.
>>> RVGL (Makeitgood)
>>> Dummiesman's Track Editor (Unity)

Thanks and credits:
>>> Acclaim and Probe for the original game.
The building textures on the texture file f are from Airport 2 by Xarc.
The left texture on the texture file h is from www.openfootage.net (the texture has been adapted) and is under Creative Commons Attribution 4.0 International License. Go to https://creativecommons.org/licenses/by/4.0/ for more information.
The door texture on the texture file i is from pngimg.com and under Creative Commons 4.0 BY-NC licence.
The skybox is the work of Humus and can be found at http://www.humus.name
It is licensed under a Creative Commons Attribution 3.0 Unported License. See http://creativecommons.org/licenses/by/3.0/

Disclaimer:
Don't use this track or its content for commercial purposes.
You can reuse any part of this track provided you mention the original authors.