Content:
>>> Install Medium Town
>>> Track information
>>> How to contact me
>>> Description
>>> Tools used
>>> Thanks and credits
>>> Disclaimer

Install Medium Town:
Unzip everything into the main folder of the game, the files will be placed where they should be.

Track information:
>>> Name: Medium Town
>>> Folder name: krnhood1
>>> Author: Keyran
>>> Year of first release: 2021
>>> Reversed: Yes
>>> Length: 2238m
>>> Reversed length: 1928m
>>> Category: Extreme
>>> Difficulty: Hard
>>> Time trial challenge: 02:44:00 normal and 02:17:00 reversed
>>> Global star: 1
>>> Practise star: 1
>>> Speedups: 0

Contact:
On Discord: I am Keyran#3667.

Description:
This is a track in a medium-sized town. Start in a toy world and explore the town!

I have started this project in April 2021. I made approximately 80% of the track during April and May 2021.
Then, I were more busy irl and also worked on other tracks. I resumed it only in October 2021.
Most of the track has been made just after I released Isivolt 1 (first version) and Lost Farm, so don't expect graphics as good as in Two Neighborhoods.

This track was originally supposed to have one or two sequels and a story, but I finally abandonned these ideas because I want to work on different tracks and I lack of motivation for this one.

The time trial times are for SEMI-PRO cars. They should be easy.

Tools used:
>>> Photofiltre for textures
>>> Blender with Marv's plugin and Blender 2.93 with Dummiesman's plugin.
>>> RVGL (Makeitgood)
>>> WorldCut
>>> Audacity

Thanks and credits:
>>> Acclaim and Probe for the original game.
I heavily reused Toy World and Toys in the Hood textures and instances (files from krnhood1a to krnhood1s).
There are some other stock textures from various tracks.
>>> Textures:
I have created the start/finish texture in krnhood1ba.bmp and the corresponding instance, please credit me if you reuse it.
Allan1: texture from Grisville in krnhood1t.bmp (upper left) and krnhood1aa.bmp
Gabor: texture from Quake! in krnhood1t.bmp (bottom left) and krnhood1ba.bmp
L17: textures from StadVolt in krnhood1aa.bmp and krnhood1ba.bmp
loafbrr: all the food assets you can find in this track and texture krnhood1ea.bmp. Link: https://loafbrr.itch.io/
If I forgot something or someone, please let me know and I will update the readme.
>>> Provided music:
"Pionners" by Marc Burt
Label: Toucan Music
http://freemusicarchive.org/
Licensed under a Attribution-NonCommercial 3.0 International License: https://creativecommons.org/licenses/by-nc/3.0/
I shortened and looped the music.
>>> rodik for testing the track and helping me with triggers before the release of this track (otherwise I would have to wait one month before releasing the track).
>>> The whole community, all those who keep the game alive, and thanks to the other creators I learned a lot while making this track.

Disclaimer:
Don't use this track or its content for commercial purposes.
You can reuse any part of this track provided you mention the original authors.