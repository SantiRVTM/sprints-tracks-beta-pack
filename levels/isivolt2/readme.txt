Content:
>>> Install Isivolt 2
>>> Track information
>>> Changelog
>>> How to contact me
>>> Description
>>> Known issues
>>> Tools used
>>> Thanks and credits
>>> Disclaimer (PLEASE READ IT IF YOU'RE A TRACK MAKER, THERE IS A TEXTURE THAT YOU'RE NOT ALLOWED TO REUSE)

Install Isivolt 2:
Unzip Isivolt 2 into the main folder of the game.

Track information:
>>> Name: Isivolt 2
>>> Folder name: isivolt2
>>> Author: Keyran
>>> Reversed: Yes
>>> Length: 988m
>>> Reversed length: 970m
>>> Category: Sprint Track
>>> Difficulty: Extreme
>>> Time trial challenge: 01:20:000 normal and 01:20:000 reversed
>>> Global star: 1
>>> Practise star: 1
>>> Speedups: 0

Changelog:
Version 2.0:
+ Sprint version of Isivolt!

Version 2.0.1:
+ Moved the start position because it wasn't very good for Time Trial.
+ Reduced the intensity of the force fields in the office.
+ Added different force fields and a different isivolt2r texture in the reverse version.
+ Modified the properties of the sand, it should be slightly easier to drive on it.

Contact:
On Discord: I am Keyran#3667.
On The Re-Volt Hideout: I am also Keyran

Description:
>>> Last year <<<
After some races in Isivolt, you want to return there for more fun. You also want to learn more about this place.
When you arrive in front of the main door, it doesn't open. It is the same for all the doors.
The security noticed the chaos you brought thanks to the cameras.
You need to find another way to enter Isivolt, and it must be when there is nobody inside it.

>>> Present day <<<
The Toy-Volt cars gathered enough money to rent a plane for one day.
What is their crazy idea? Play Isivolt 2 to discover it!
What if the staff, who think they will never be disturbed by R/C cars again, forgot to close a door?

>>> What are the new rooms? <<<
In the new areas, each room is inspired by a subject that students can learn at the real-life school that inspired the track.
You will have to avoid some sumo-robots, drive between servers and play a game I created in a unique room.
Be careful, someone may take the control of your car!

More explanations about the subjects that are not obvious:
>>> Embedded systems: in this track, these are the sumo-robots.
>>> Operational research: in this track, this is the small factory. Operational research is a large subject mostly about optimization, sometimes this is optimization of the process in a factory.
>>> Data science: the hardest to understand here. Each white cross is supposed to be coordinates collected during a race, and then these coordinates are affected to one of the racelines (there are several sets of coordinates).
With more coordinate sets it could be possible to use data science tools to guess to which raceline belong a unknown set of coordinates.
The game with the colors could be for image analysis (not mentioned in the track), and gives a unique atmosphere to this room.
>>> Networks and security: to make something about this topic, I had the idea to make players feel like someone took the control of their car.

>>> Why the last part? <<<
I originally wanted to add a whole underground maze for the practice mode, with some elevators that bring the car to different levels (yes, elevators again)
I removed all those parts because I wasn't motivated to polish areas made only for the practice mode.
There is still a lava river and an empty underground room (the finish line), because Isivolt is located close to a volcano. It is inactive since 10.000 years, it shouldn't wake up.
That explains the choice of the skybox in Isivolt 1 (the mountains are supposed to be old inactive volcanoes).

>>> Time trial:
It is for Super Pro cars. For a harder challenge, try with a good Pro car.

Known issues:
Texture animation reflection bug in the server room, it is an old Re-Volt bug.
Luckily the texture that indexes the id of the animation has a big pureblack area, so I used it.

Tools used:
>>> Photofiltre 7 for textures
>>> Blender with Marv's plugin
>>> RVGL (Makeitgood)
>>> Worldcut

Thanks and credits:
>>> Acclaim and Probe for the original game.
>>> Music:
"On Target (remix)" by JMD
Label: Toucan Music
http://freemusicarchive.org/
Licensed under a Attribution-NonCommercial 3.0 International License: https://creativecommons.org/licenses/by-nc/3.0/
>>> Gabor for Quake! textures and instances (I modified some of them).
>>> Xarc for the camera from Mysterious Toy-Volt Factory 1.
>>> Kiwi for the black board from School's Out! tracks.
>>> Billster for the picnic area from PetroVolt PRM set.
If I forgot something or someone, please let me know and I will update the readme.
>>> New textures:
isivolt2d: PLEASE DO NOT REUSE THE ISIMA LOGO FOR YOUR CARS AND TRACKS.
isivolt2i: under the 2 stock textures, there is a modified Quake! texture (need to credit both Gabor and me), the other textures are mine (please credit me if you use them).
isivolt2j, isivolt2q: likely a free texture, don't need to credit anybody.
isivolt2k: bell icon: author: https://www.freepik.com downloaded from https://www.flaticon.com/ 
isivolt2m, isivolt2n, isivolt2o and isivolt2p (top): public domain, downloaded from https://polyhaven.com/
isivolt2p (bottom): ASK ME IF YOU WAN'T TO REUSE THE SCREEN TEXTURE.
isivolt2r: I don't believe anyone will reuse it. Please, don't reuse it, make your own texture or ask me, it will be better for players.
isivolt2s and isivolt2t: You can make your own skins, provided you don't include the Isivolt logo.
isivolt2v (bottom): scratch made by me, you can reuse this provided you credit me.
isivolt2x and isivolt2y: Mostly text, don't reuse anything from there without asking me.
>>> New models (and models from Isivolt 1):
Every part of the tramway is made from scratch by me (and also the pole and wire instances).
The plane is scratch made by me. It is far from perfect, but correct for what I do with it.
You can reuse them and modify them if you want as long as you credit me for the original models.
The new monitors (3 and 4) are scratch made by me.
You must ask me for permission if you use it "as is".
You don't need to ask me for permission if you use a different texture on the screen.
Instances sign1 to sign6 (in both Isivolt tracks) are instances with modified textures from the stock Museum tracks.

The Isivolt logo has been made for this track. I wanted a logo based on the real logo of the school, and the communication manager of the school made it for me.
You can use it only in a video of the track.

Disclaimer:
Don't use this track or its content for commercial purposes.
Don't upload this track (modified or not) anywhere outside Re-Volt community websites without my approval. All websites listed in #welcome on the Re-Volt discord server are OK.
Don't upload a modified version of the track anywhere. I you want to suggest me an update for this track, contact me on Discord, I may include it if I have to update the track.