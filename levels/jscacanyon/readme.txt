Name: JSCA Canyon
Authors: Josh Scorpius
Length: 2,250 metres (1.398 miles)

This is the canyon at night. A version I created to make it similar such as the Need for Speed Carbon game from the year 2006. This is the european version.

Contains:
Pickups
Triggers
Custom camera nodes
Reversed mode

Any case, contact at: 
Josh Scorpius: joshscorpius94@gmail.com