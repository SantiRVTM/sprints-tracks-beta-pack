                  *******************
                  * ~ Space Opera ~ *
                  *******************
 
"Somewhere in space, in an RC stadium where gravity is less, a race is organized. Only the best can compete in this hostile place!"
                  
Length: 1736m
Difficulty: Hard

This track was created for the Birthday Contest 2023 (which took place from March 17 to April 1) to celebrate the second birthday of Re-Volt World.
This year, the theme of the track category was to create a Lego or Lego Extreme Track which is a Sprint Track.
Space Opera is the result of my contribution.
____________________________________________________

I used the Track Editor (Unity Edition) by Dummiesman _ https://www.revoltworld.net/dl/track-editor-unity-edition/ for the raceline, and MAKEITGOOD for the rest.
Instances & Object are from the Original Re-Volt Game. No other additions were used. 
A big thank you to Rodik for having created a new texture page for the Toy World instances in his excellent "Makeitscary" track & for the UFO too (-;
The images used for textures and skybox are either in the public domaine or free (according to the research site used) and modified whith Gimp. I don't know the names of their authors.
Music "Aria of the Sun" composed by Rafael Krux (feat Andrea Krux) under license CC BY 4.0.

____________________________________________________

Driving with low gravity can seem confusing, but you get used to it quickly. The best is to choose a car that is not too nervous, without too much acceleration (RC Bandit is perfect to me). And it's also necessary to anticipate a lot!
I hope you enjoy this track,
thank you for downloading it (-;
____________________________________________________
