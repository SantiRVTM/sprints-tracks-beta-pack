===TRACK SUMMARY===

Name:          SWR: Zugga Challenge
Length:        3495 meters
Difficulty:    Medium
Converted by:  mrroblinx & Matsilagi
Creation date: September 22nd to October 6th 2021

===DESCRIPTION===

Another track from Star Wars Episode I Racer. This time it's a really long one, much longer than the previous one; so if you thought that one was boring be prepared for this one... You should definetly be using fast cars for this - Super Pros (maybe Pros) only, unless you want to be here all day.
Just like the previous track, there aren't any pickups, and I'm keeping it that way. Took a long time to make the animated objects of this, and a lot of other stuff too. I hope you enjoy it.

===INCLUDED IN THIS TRACK==

Practice Star
Challenge Time
Custom animations (Starting laser, Cranes, Drones, Digging machines, Ships)

===TOOLS USED===

Blender (with Marv's plugin)
Paint.NET

===SPECIAL THANKS===

Matsilagi, for providing me with the assets and beta testing.
The Re-Volt community.