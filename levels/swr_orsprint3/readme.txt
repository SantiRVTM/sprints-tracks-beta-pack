===TRACK SUMMARY===

Name:          SWR: Abyss
Length:        1781 meters
Difficulty:    Extreme
Converted by:  mrroblinx & Matsilagi
Creation date: September 19th to 22nd 2021

===DESCRIPTION===

Star Wars Episode I Racer is one of my favorites games of all time, and I always wanted to convert some tracks from this game. Now the opportunity has finally shown itself.
Matsilagi requested me to make this, and since he was so kind to give me all of the assets I needed (models, textures, etc), I was able to convert the track with flying colors.
Abyss is my favorite track from the original game due to its infamous nature... I feel deeply connected to it as I've have raged and raged in the past.
There are no pickups in my conversion, as I felt it would've been a clusterfuck in the tunnels (and I was very lazy). The original game didn't have any so in a way it's accurate.
Super Pros or any fast cars are recommended for this track as it can be pretty long, especially if you fall off to the wrong route...

===INCLUDED IN THIS TRACK==

Practice Star
Challenge Time
Custom animations (Blimp, Starting laser)

===TOOLS USED===

Blender (with Marv's plugin)
Paint.NET

===SPECIAL THANKS===

Matsilagi, for providing me with the assets and beta testing.
The Re-Volt community.