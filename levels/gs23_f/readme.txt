Made by Erzu in May 2023
First introduced in Game Show Session in June 2023 as the final event
Track name in-game: Duel
Folder name: gs23_f

This track is a Symmetric Lego Sprint Track made for 1v1 racing.
There are no pickps and also no AI nodes so repoing is not possible!