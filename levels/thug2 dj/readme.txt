Track name: Downhill Jam (THUG2)
Install in folder: Re-volt\levels\THUG2 DHJ
Author: Mace (aka: Mace121)
Email Address: Macethe0mni@gmail.com


Description:

The Downhill Jam. One of the most infamous Tony Hawk levels where it's a linear affair full of pipes, gaps, and boxes. The original THPS idea was meant to be like a racing game, like Sega's Top Skater. There IS an official TH Racing game that's named after this level, which was based off the famous Hoover Dam between Arizona & Nevada. This is an experimental track, and maybe mark the very first conversion designed to be a 1-lap track.

There are several incarnations of Downhill Jam throughout the franchise, as it keeps making appearances:

About the track being infamous? Well, this is the Creed (the band), Keemstar, or Adam Sandler of Tony Hawk levels, despite the fact there are far worse levels in the TH franchise... Like Rooftops in THPS2X, or the entirety of TH American Sk8land on GBA. Personally, I liked this level. The main draw of the criticism was it's one main gimmick: It's linear, especially in the original versions where you had to reatart the level to do it all over again... But in Re-Volt, that's the part of the new-found fun. 
______________________________________________________________________________________

Some Info:

-Track length: 500m (1-lap)

Music:
-This time, there's none. Only Redbook.


The Mixtape (in-development):
-Powerman 5000: When Worlds Collide
-Dead Kennedys: Police Truck
-Blood: God Like
-Rigoletto: La Donna e Mobile
-Fu Manchu: California Crossing


Misc.

This is much easier to make than Frigid Peaks.

Notable changes:
-The Quarterpipes & halfpipe have higher polycounts
-Some textures are changed, one is replaced entirely, due to it's odd format. 
-The entire texture set is remapped, most of them retain it's original looks, while the bigger ones are completely remapped due to their looping texture properties.
-The puddles & crates are present. Others are removed.
-No NPCs, but that's a theme in Re-Volt.
-The halfway point (the one after the halfpipe) faced reconstruction to better suit with the cars.
-Vertex shading is altered due to the sun in the skybox is facing
-Custom skybox & sounds are added

______________________________________________________________________________________

* Copyright / Permissions *

Track models & Textures by Activision & Neversoft (R.I.P.)
Blender Plugin (to rip models) by the THPSX community
Custom sounds from Freesound

Yeah, sure. Why not, unless you credited the author(s).
