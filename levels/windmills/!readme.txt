﻿Content:
>>> Install Windmills Hills Sprint
>>> Track information
>>> Changelog
>>> How to contact me
>>> Description
>>> Time trial
>>> Additional information
>>> Known issues
>>> Tools used
>>> Thanks and credits
>>> Disclaimer

Install Windmills Hills Sprint:
Unzip Windmills Hills Sprint into the main folder of the game.

Track information:
>>> Name: Windmills Hills Sprint
>>> Folder name: windmills
>>> Author: Keyran
>>> Year of first release: 2023
>>> Year of last update: 2024
>>> Reversed: Yes
>>> Length: 2019m
>>> Reversed length: 1999m
>>> Category: Kit Track
>>> Difficulty: Hard
>>> Time trial challenge: 02:48:000 normal and 02:50:000 reversed
>>> Global star: 1
>>> Practise star: 1
>>> Speedups: 0

Changelog:
Version 3.0.1:
+ Used a new tree model (optimized and looks better compared to the previous one).

Contact:
On Discord: I am keyran_rv.

Description:
See this as Windmills Hills 3 (hence the version 3). It's a test track for the new tiles in my Rally PRM Kit.

Time trial:
The time trial challenges are for Semi-Pro cars. For a harder challenge, try with an Advanced car (tested with panga TC).
Fun fact, for the reverse version, my best time with Panga TC is exactly the challenge time. I did some minor mistakes, it could be better.

Additional information:
This section is for those who like reading (although you probably downloaded this to play the track).
I recommend reading this AFTER playing this track and the previous ones, because I will mention some parts of the tracks here.

Windmills Hills 3 (WH3) is a Sprint track, and a mix between what Windmills Hills 2 (WH2) was supposed to be originally (before the event mentioned in the story happened to me) and a new track idea.
Yes, WH2 should have been a long sprint track, and I wanted to use a lot of hills, maybe more than what WH3 has.

WH2 was originally supposed to have a jump over the entrance of a tunnel in the main raceline. There is such a jump in WH3, in a shortcut.
In my original idea for WH2, the road with the tunnel would have been used in both directions. I haven't followed this idea in WH3.

About technical stuff, WH3 shows some technical limitations that I reached: some parts are exported as instances, but I have used more than 1024 tiles. For this reason, some parts of the track are exported in the world file.
I haven't exported the whole track in the world and collision files because they would have had too many polys. If you open the world, collision or fin file in Blender, you would be surprised by what you would see.
I share the strategy I followed, in case someone encounters the same issue: I have exported low poly instances in the world file and heavy instances and decorative objects in the instance file. I have also manually optimized collision in the collision file by removing some useless edges.

Known issues:
This is a large open world with a lot of trees that have a translucent texture, it won't work smoothly if you don't have a good computer.

Tools used:
>>> Photofiltre for textures
>>> Blender with Marv's plugin.
>>> RVGL (Makeitgood)
>>> Visual Studio Code and the notepad for editing the readme, properties, custom animations and the inf file.

Thanks and credits:
>>> Acclaim and Probe for the original game.
The bush instance and texture are from Botanical Garden.
The leaves on the texture file f are from https://www.pngall.com/leaves-png/download/1721 and under licence Creative Commons 4.0 BY-NC.
The texture has been modified (different size and added vertical symmetry, also added the black background on the bitmap version).
The left texture on the texture file h is from www.openfootage.net (the texture has been adapted) and is under Creative Commons Attribution 4.0 International License. Go to https://creativecommons.org/licenses/by/4.0/ for more information.
The door texture on the texture file i is from pngimg.com and under Creative Commons 4.0 BY-NC licence.
>>> Provided music:
"Turbulence (2020 remix)" by Marc Burt
http://freemusicarchive.org/
Licensed under a Attribution-NonCommercial 4.0 International License: https://creativecommons.org/licenses/by-nc/4.0/

Disclaimer:
Don't use this track or its content for commercial purposes.
You can reuse any part of this track provided you mention the original authors.