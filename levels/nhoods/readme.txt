Content:
>>> Track information
>>> Changelog
>>> Description
>>> Contact
>>> Tools used
>>> Textures used and thanks
>>> Known issues
>>> Disclaimer

Track information:
>>> Name: Two neighborhoods
>>> Folder name: nhoods
>>> Author: Keyran
>>> Year of first release: 2021
>>> Year of last update: 2022
>>> Last readme update: 2023
>>> Reversed: Yes
>>> Length: 1408m
>>> Reversed length: 1429m
>>> Category: Sprint Track
>>> Difficulty: Extreme
>>> Time trial challenge: 01:38:000 (very easy with Super-Pro cars, easy or medium with Pro cars, hard with good Semi-Pro cars)
>>> Time trial challenge (reversed): 01:40:000 (very hard with Semi-Pro cars)
>>> Global star: 1
>>> Practise star: 1
>>> Speedups: 0

Changelog:
Version 1.1.1:
+ Fixed a z-fighting issue in a window.
+ Improved shading in the bedroom.

Description:
This track uses Toys in the Hood tracks as a base. I haven't modified them a lot. I have added a road that joins the two original tracks.
I finished the road close to the start line in Toys in the Hood 1 and I modified some surface properties in Toys in the Hood 2.

This track has been made for the Re-Volt World Summer Contest 2021. It finished 3rd, behind Toy World 3 (by javildesign, the winner) and Déréalisation (by Kiwi, 2nd).

There is an optional bonus. It adds some Phat Slugs crossing the road at different places, and a exclusive road sign.
To enable it, launch the game with the pack "phat_trolley" provided in the download (command line: rvgl.exe -packlist phat_trolley).

The track requires rvgl19.1230a or newer (number of pickups in "properties.txt"). The bonus requires rvgl20.1230a or newer (packs).

Contact:
>>> Discord: keyran_rv

Tools used:
>>> Photofiltre 7 for textures
>>> Blender with Marv's plugin
>>> RVGL (Makeitgood)
>>> WorldCut

Textures used and thanks:
Textures a, b, c, d, e, f, g, h, i, j, k, l, p, q and u are stock textures.
Textures m, n, r, s, t, v and x are likely in public domain (I haven't found any restriction on them). I modified texture r.
Texture o has stock textures, and on the left, a texture made by myself from textures m and n.
Texture w is scratch made by me and contains the rvgl logo.
[Bonus pack] Texture y is made by me and contains Phat Slug.
All instances and models are either stock or made by me. Some stock models are slightly modified to take into account the location of their corresponding textures.

Thanks Paperman who changed the colors of most of the new textures and edited vertex painting in the world file, the national roads, the grass and the cliff look better.
Thanks Acclaim / Probe for the original game, and the community that keeps the game alive.

Known issues:
>>> [Bonus pack] When a Phat Slug is created, the water texture disappear in the Toys in the Hood 2 river.

Disclaimer:
Don't use any part of this track for commercial purposes (except the textures m, n, r, s, t, v and x).