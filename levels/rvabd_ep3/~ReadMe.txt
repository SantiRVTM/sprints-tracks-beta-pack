                  ********************************************************
                  * ~ Re-Volt Abduction - Episode 3: Back to the Earth ~ *
                  ********************************************************
                  
                     "We manage to escape, but will we make it home?"
                  
                  
_________________________________________________                  
 
Here is the third and last track of my second original cup: "Re-Volt Abduction".

Author: CED!
Creation: From November 2023 to February 2024
Category: Sprint Track                  
Length: 698 meters           
Difficulty: hard        
Practice Star: 1
Time Trial: 00:58:045
_________________________________________________
  
I used the Rally PRM Set by Keyran to create the ground, the loophole, the trap and the walls.
Mountains are from the Off-Road Arena Kit by JimK. They have been resized.
Tree trunks & signs come from "Moon Dawn" by Allan1.
Aliens & ray of light under the ufo come from the track "Unit 51" by Josh PIN.
And finally, the pillars under the houses were made with a piece of the lego pack by Killer Wheels.

The sound "Alien Communication" comes from Joseph Sardin's site https://lasonotheque.org/.

I composed the music: "440" under the Creative Common licence CC-BY-NC-ND 4.0, but you can use it freely in any other Re-Volt project (and please: don't forget to credit me).
__________________________________________________

WARNING: this track have low gravity and it can seem confusing to drive. but you get used to it quickly. The best is to choose a car that is not too nervous, without too much acceleration (Toyeca is perfect to me). And it's also necessary to anticipate a lot!
__________________________________________________

This "readme file" was achieved with the help of an online translator. Sorry if English is approximate, it would have been worse without.

I hope you enjoy this track,
thank you for downloading it (-;

Thanks to 607, Keyran & Rodik for testing this track and for the feedback.

Special thanks to Acclaim, the RVGL Team, all tracks & cars creators, and all other re-volt contributors .../... past, present & futur...

Don't use this track or its content for commercial purposes.
You can reuse any part of this track provided you mention the original authors.
__________________________________________________
