Name: JSCA Canyon 3
Authors: Josh Scorpius
Length: 2,332 metres (1.449 miles)
Started: Sunday, January 3, 2021
Terminated: Monday, January 4, 2021
Difficulty: Hard

This is the canyon at early morning. A version I created to make it similar such as the Need for Speed Carbon game from the year 2006. This is the UK version, speed is in miles per hour (1 mile = 1.609344 km). Third version of the canyon and a little bit longer than the JSCA Canyon released on the 2020 season.

Contains:
Pickups
Triggers
Custom camera nodes
Reversed mode

Any case, contact at: 
Josh Scorpius: joshscorpius94@gmail.com or at Discord to Josh Scorpius#8881.