Andorra (Sprint 1) - 759m
Andorra (Sprint 2) - 664m
ARC Tappa 1 - 1215m
ARC Tappa 1 SS1 - 269m
ARC Tappa 1 SS2 - 341m
ARC Tappa 1 SS3 - 314m
ARC Tappa 1 SS4 - 289m
ARC Tappa 2 - 2408m
ARC Tappa 2 SS1 - 810m
ARC Tappa 2 SS2 - 510m
ARC Tappa 2 SS3 - 546m
ARC Tappa 2 SS4 - 529m
ARC Tappa 3 - 1567m
ARC Tappa 3 SS1 - 395m
ARC Tappa 3 SS2 - 314m
ARC Tappa 3 SS3 - 367m
ARC Tappa 3 SS4 - 483m
Autumn Castle - 1125m
Bruma Forest - 1931m
Canyon Road - 1313m
Downhill Jam (THUG2) - 498m
Duel - 927m
Foggy Memory - 1280m
Fortress Sprint - 751m
Gela Hillclimb - 1805m
Highrise - 812m
Into The Cargo Plane - 1377m
Isivolt 2 - 988m
JSCA Big Island Sprint - 592m
JSCA Canyon - 2250m
JSCA Canyon 2 - 1750m
JSCA Canyon 3 - 2332m
JSCA Canyon 4 - 2295m
JSCA Canyon 5 - 1703m
JSCA Canyon 6 - 2002m
JSCA Canyon 7 - 1478m
JSCA Canyon 8 - 1312m
JSCA Canyon 9 - 623m
JSCA OutRun RV Route A - 1354m
JSCA OutRun RV Route B - 1305m
JSCA OutRun RV Route C - 1410m
JSCA Prehistoric Dash - 512m
JSCA Rainbow Road - 383m
JSCA Rainbow Road 2022 (Sprint) - 911m
JSCA Russian City (Sprint) - 518m
JSCA Sprint Lego Track - 924m
Lego Junction - 2878m 
Magnum Valley (Sprint) - 590m
Medium Town - 2238m
MiniGame: FroggerVolt Easy - 66m
MiniGame: FroggerVolt Hard - 66m
MiniGame: Lost - 260m
Night City Area 2 (Sprint) - 435m
Night City Area 2 (Sprint 2) - 678m
Phantasmabyssal Turbulence - 1765m
Re-Volt Abduction (Episode 1) - 384m
Re-Volt Abduction (Episode 3) -698
Ruddles County - 5796m
Sideral-lye - 4006m
Snowy Stage - 2845m
Snowy Stage SS1 - 608m
Snowy Stage SS2 - 763m
Snowy Stage SS3 - 842m
Snowy Stage SS4 - 622m
Space Opera - 1736m
SWR: Abyss Sprint - 1781m
SWR: Zugga Sprint - 3495m
Touge Mountain - 2502m
Touge Mountain - Stage 1 - 1136m
Touge Mountain - Stage 2 - 1357m
ToyChaos2 - 3388m
Two neighborhoods - 1408m
Vuokkari Stadium - 1493m
Windmills Hills Lego - 894m
Windmills Hills Sprint - 2019m
Zooming Paradise - 1172m